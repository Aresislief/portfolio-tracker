import copy
import datetime
import os
import pandas as pd

from globalParams import *

def getNowString():
    now = datetime.datetime.now()
    nowstr = now.strftime(DATETIME_FORMAT)
    return nowstr


class TransactionLogger:
    def __init__(self, logname, create=False):
        self.loggerFile = logname
        self.homeDir = os.path.dirname(__file__)
        self.logDir = os.path.join(self.homeDir, 'logs')
        self.logPath = os.path.join(self.logDir, logname)
        self.txLog = None

        self.__connectToLog(create)

    def __connectToLog(self, create):
        # Check if the log is old and possibly back it up
        if not os.path.isdir(self.logDir):
            os.mkdir(self.logDir)

        if os.path.isfile(self.logPath):
            with open(self.logPath, 'r') as f:
                txCount = len(f.readlines()) - HEADER_COUNT
            print("Connected to log file {}.\nIt contains {} transactions".format(self.logPath, txCount))
            # TODO: Get the most recent time and if it is rather old back it up

        elif create:
            with open(os.path.join(self.logDir, self.loggerFile), 'w') as f:
                f.write('Transaction log created on: {}\n'.format(getNowString()))
                f.write("Type,Quantity,Coinpair,Price,Time\n")
            print('New transaction log created in {}'.format(os.path.join(self.logDir, self.loggerFile)))

    def __backupTxLog(self):
        # Back up the Tx log
        raise NotImplementedError

    def addTx(self, type, quantity, coinpair, price, time=None):
        # Add transaction to the log
        if time is None:
            nowString = getNowString()
        else:
            nowString = time
        txLine = "{},{},{},{},{}\n".format(type, quantity, coinpair, price, nowString)
        with open(self.logPath, 'a') as log:
            log.write(txLine)

    def getTx(self, type=None, quantityRange=None, coin=None, priceRange=None, timeRange=None):
        # pull up some or all transactions from the log
        if self.txLog is None:
            self.getFullLog()

        selectedTx = copy.deepcopy(self.txLog)
        if type:
            assert type.lower() == 'buy' or type.lower() == 'sell'
            selectedTx = selectedTx[selectedTx['Type'].str.lower().eq(type.lower())]

        if quantityRange:
            selectedTx = selectedTx[selectedTx['Quantity'] > quantityRange[0]]
            selectedTx = selectedTx[selectedTx['Quantity'] < quantityRange[1]]

        if coin:
            selectedTx = selectedTx[selectedTx['Coinpair'].str.contains(coin)]

        if priceRange:
            selectedTx = selectedTx[selectedTx['Price'] > priceRange[0]]
            selectedTx = selectedTx[selectedTx['Price'] < priceRange[1]]

        if timeRange:
            dateTimesLog = pd.to_datetime(selectedTx['Time'], format=DATETIME_FORMAT)
            selectedTx = selectedTx[dateTimesLog > date1]
            selectedTx = selectedTx[dateTimesLog < date2]

        return selectedTx

    def getFullLog(self):
        # Returns all the transactions
        self.txLog = pd.read_csv(self.logPath, header=HEADER_COUNT - 1)
        return self.txLog


def runExample():
    testLogger = TransactionLogger('test.log', create=True)
    testLogger.addTx('Buy', 1.5, 'BTC/USD', '40000', time="05/01/1970 01:02:03")
    testLogger.addTx('Sell', 0.1, 'ETH/EUR', '3000')
    testTx = testLogger.getTx(quantityRange=[0.5, 2])
    testTx2 = testLogger.getTx(coin='ETH')
    testTx3 = testLogger.getTx(priceRange=[100, 50000])

    date1 = datetime.datetime.strptime("01/01/1970 01:02:03", DATETIME_FORMAT)
    date2 = datetime.datetime.strptime("08/02/1970 01:02:03", DATETIME_FORMAT)
    testTx4 = testLogger.getTx(timeRange=[date1, date2])

    testTx5 = testLogger.getTx(type='SELL')
    print('Quantity:\n{}'.format(testTx))
    print('Coin:\n{}'.format(testTx2))
    print('Price:\n{}'.format(testTx3))
    print('Time:\n{}'.format(testTx4))
    print('Type:\n{}'.format(testTx5))


if __name__ == '__main__':
    runExample()
