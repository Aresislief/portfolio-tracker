import numpy as np


class MessageGenerator:
    def __init__(self, user, value, currency, profit):
        self.__user = user
        self.__value = value
        self.__currency = currency
        self.__profit = profit  # Boolean that sets positive or negative messages
        self.message = ''

        self.__posPreambles = ['Wow!', 'Guys look!', 'Amazing!', 'Such profit!',
                               'Damn!', 'Wubbalubbadubdub!', 'Dude!', 'Look at this!', 'Impressive!']
        self.__negPreambles = ['Ahw', 'Sad...', 'It will get better, but', 'Oh man', 'Chin up',
                               'There is a slight dip...', 'Oops!', 'Oepsiewoepsie', 'rip...']

        self.__posEmojis = ['\U0001F525', '\U0001F680', '\U0001F319', '\U0001F48E', '\U0001F44F', '\U0001F4C8']
        self.__negEmojis = ['\U0001F614', '\U0001F622', '\U0001F61F']

        self.__firstDogeList = ['Such', 'Very', 'Much', 'Many']
        self.__posDoge = ['wow!', 'profit', 'crypto', 'invest', 'money', 'hedgefund', 'J&J', 'stocks', 'stonks', 'cash',
                          'magic internet money']
        self.__negDoge = ['sad', 'cry', 'tear', 'hodl', 'dip', 'no profits', 'buying opportunity']

    @property
    def __preamble(self):
        preambleList = self.__posPreambles if self.__profit else self.__negPreambles
        preambleNum = np.random.randint(len(preambleList))
        return preambleList[preambleNum]

    @property
    def __emoji(self):
        emojiList = self.__posEmojis if self.__profit else self.__negEmojis
        emojiNum = np.random.randint(len(emojiList))
        return emojiList[emojiNum]

    @property
    def __dogeMeme(self):
        index1 = np.random.randint(len(self.__firstDogeList))
        secondDogeList = self.__posDoge if self.__profit else self.__negDoge
        index2 = np.random.randint(len(secondDogeList))
        return self.__firstDogeList[index1] + ' ' + secondDogeList[index2]

    def generateMessage(self):
        self.message = ''
        for i in range(np.random.randint(4)):
            self.message += ' ' + self.__emoji
        self.message += ' ' + f"{self.__preamble}"
        self.message += ' ' + f"{self.__user}'s portfolio is now worth {self.__value:.2f} {self.__currency}"
        for i in range(np.random.randint(4)):
            self.message += ' ' + self.__emoji
        self.message += ' ' + self.__dogeMeme
        self.message += ' ' + self.__dogeMeme

        return self.message
