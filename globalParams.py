PORTFOLIO_HEADER_COUNT = 2
HEADER_COUNT = 2
DATETIME_FORMAT = "%d/%m/%Y %H:%M:%S"

TICKER_NAME_LOOKUP = {'BTC': 'bitcoin',
                      'ETH': 'ethereum',
                      'VET': 'vechain',
                      'LINK': 'chainlink',
                      'MATIC': 'matic-network'}

PORTFOLIO_COIN_NAME_LOOKUP = {'bitcoin': 'BTC',
                              'ethereum': 'ETH',
                              'vechain': 'VET',
                              'chainlink': 'LINK',
                              'matic-network': 'MATIC'}