import datetime
import json
import os
import time
import pandas as pd
from pycoingecko import CoinGeckoAPI

from plotly.offline import plot
import plotly.graph_objects as go

from transactionLogger import TransactionLogger, getNowString, DATETIME_FORMAT
from globalParams import *


def initializeCoinDict(coin, value=None):
    coinDict = {}
    coinDict[coin] = {}
    coinDict[coin]['holding'] = 0.

    return coinDict


class PortfolioManager:
    def __init__(self, portfolioName, create=False):
        self.portfolioName = portfolioName
        homeDir = os.path.dirname(__file__)
        self.portfolioDir = os.path.join(homeDir, 'logs')
        self.portfolioPath = os.path.join(self.portfolioDir, portfolioName)
        self.portfolio = None
        self.txLogger = None
        self.cg = CoinGeckoAPI()
        self.__USDEUR = None
        self.allMarketsHistory = {}
        self.historicalPortfolioValue = None

        self.__connectToPortfolioLog(create)

    def __connectToPortfolioLog(self, create):
        # Check if the log is old and possibly back it up
        if not os.path.isdir(self.portfolioDir):
            os.mkdir(self.portfolioDir)

        if not os.path.isfile(self.portfolioPath) and create:
            with open(self.portfolioPath, 'w') as pfFile:
                # testDict = initializeCoinDict(coin="DUMMY")
                # testDict2 = initializeCoinDict(coin="DUMMY2")
                # fulldict = {**testDict, **testDict2}
                initDict = {}
                initDict["LastUpdated"] = getNowString()
                json.dump(initDict, pfFile)
            print('New portfolio log created in {}'.format(self.portfolioPath))

        if os.path.isfile(self.portfolioPath):
            with open(self.portfolioPath, 'r') as pfFile:
                self.portfolio = json.load(pfFile)

    def fullPortfolio(self):
        # Returns the full portfolio
        return self.portfolio

    def show(self):
        print(self.portfolio)

    def updatePortfolio(self, txLogPath, force=False):
        # Update the portfolio using a transactionlog,
        # only updating if the transactions are more recent
        self.txLogger = TransactionLogger(txLogPath)
        allTx = self.txLogger.getFullLog()

        lastPortfolioUpdate = datetime.datetime.strptime(self.portfolio['LastUpdated'], DATETIME_FORMAT)
        newTime = lastPortfolioUpdate
        newTxCounter = 0
        for ind, tx in allTx.iterrows():
            # Determine whether or not this transaction should occur based on the time
            txTime = datetime.datetime.strptime(tx['Time'], DATETIME_FORMAT)
            if txTime > lastPortfolioUpdate or force:
                # Do the update
                sign = 1 if tx['Type'].lower() == 'buy' else -1
                coinPair = tx['Coinpair'].split('/')
                if coinPair[0].upper() in self.portfolio.keys():
                    self.portfolio[coinPair[0].upper()]['holding'] += sign * tx['Quantity']
                else:
                    # New holdings are added
                    self.portfolio[coinPair[0]] = {}
                    self.portfolio[coinPair[0]]['holding'] = sign * tx['Quantity']

                if coinPair[1].upper() in self.portfolio.keys():
                    self.portfolio[coinPair[1].upper()]['holding'] -= sign * tx['Quantity'] * tx['Price']
                else:
                    # New holdings are added
                    self.portfolio[coinPair[1]] = {}
                    self.portfolio[coinPair[1]]['holding'] = -sign * tx['Quantity'] * tx['Price']

                # Keep track of time, but not replace yet to prevent locking out further transactions.
                if txTime > newTime:
                    newTime = txTime

                newTxCounter += 1

        self.portfolio['LastUpdated'] = datetime.datetime.strftime(newTime, DATETIME_FORMAT)
        with open(self.portfolioPath, 'w') as pfFile:
            json.dump(self.portfolio, pfFile)

        print("{} new transactions were added".format(newTxCounter))

    def __calculatePortfolioValueUSD(self):
        totalUSDValue = 0.
        # build the api call
        cgIDs = []
        for coin in self.portfolio.keys():
            if coin != 'LastUpdated' and coin != 'USD' and coin != 'EUR':
                try:
                    cgID = TICKER_NAME_LOOKUP[coin]
                    cgIDs.append(cgID)
                except KeyError:
                    print('{} not found in the coingecko lookup'.format(coin))

        # Execute call
        current_prices = self.cg.get_price(ids=cgIDs, vs_currencies='usd')

        # Calculate total holding
        for cgID in cgIDs:
            coinname = PORTFOLIO_COIN_NAME_LOOKUP[cgID]
            totalUSDValue += current_prices[cgID]['usd'] * self.portfolio[coinname]['holding']

        return totalUSDValue

    @property
    def USDToEUR(self):
        if self.__USDEUR is None:
            USDEUR = self.cg.get_price(ids='tether', vs_currencies='eur')
            self.__USDEUR = USDEUR['tether']['eur']
        return self.__USDEUR

    def getTotalValue(self):
        totalUSDValue = self.__calculatePortfolioValueUSD()
        totalEURValue = totalUSDValue * self.USDToEUR
        return totalUSDValue, totalEURValue

    def __convertUnixToDatetime(self, unix):
        dateTimeStamp = datetime.datetime.utcfromtimestamp(unix/1000)
        return dateTimeStamp

    def getHistoricalPriceDataByCoin(self, cgId, timestart, timeend):
        timeStartUnix = timestart.timestamp()
        timeEndUnix = timeend.timestamp()

        marketchart = self.cg.get_coin_market_chart_range_by_id(cgId, 'usd', timeStartUnix, timeEndUnix)
        for ind, pricelist in enumerate(marketchart['prices']):
            pricelist.append(self.__convertUnixToDatetime(pricelist[0]))

        return marketchart

    def requestHistoricalPrices(self):
        for coin in self.portfolio.keys():
            if coin != 'LastUpdated' and coin != 'USD' and coin != 'EUR':
                try:
                    cgID = TICKER_NAME_LOOKUP[coin]
                    # Execute call
                    # TODO: Make this depend on the tx log or inception date of the portfolio file
                    startTime = datetime.datetime(year=2021, month=11, day=4, hour=12, minute=0, second=0)
                    endtime = datetime.datetime.now()
                    coinMarketChart = self.getHistoricalPriceDataByCoin(cgID, startTime, endtime)
                    self.allMarketsHistory[coin] = coinMarketChart
                except KeyError:
                    print('{} not found in the coingecko lookup'.format(coin))

    def getHistoricalPortfolioPrices(self):
        assert self.allMarketsHistory != {}, "First call coingecko using requestHistoricalPrices"
        historicalPortfolioValue = []
        firstKey = list(self.allMarketsHistory.keys())[0]
        # Use just the first key to get the length of what's behind
        numTimestamps = len(self.allMarketsHistory[firstKey]['prices'])
        for timeFrame in range(numTimestamps):
            framePrice = 0
            for coin in self.portfolio.keys():
                if coin != 'LastUpdated' and coin != 'USD' and coin != 'EUR':
                    try:
                        framePrice += self.portfolio[coin]['holding'] * self.allMarketsHistory[coin]['prices'][timeFrame][1]
                    except TypeError or KeyError:
                        pass
            # We can use the firstKey again because all timestamps will the shared by the coins
            historicalPortfolioValue.append([self.allMarketsHistory[firstKey]['prices'][timeFrame][2], framePrice])

        self.historicalPortfolioValue = pd.DataFrame(historicalPortfolioValue, columns=['date', 'valueUSD'])
        self.historicalPortfolioValue['valueEUR'] = self.historicalPortfolioValue['valueUSD'] * self.USDToEUR
        return self.historicalPortfolioValue

    def plotHistoricalPortfolioValue(self):
        assert self.historicalPortfolioValue is not None, "No historical value yet. First call getHistoricalPortfolioValue"
        fig = go.Figure()
        fig.add_scatter(x=self.historicalPortfolioValue['date'], y=self.historicalPortfolioValue['valueEUR'])
        fig.update_layout({"title": 'Portfolio value',
                           "xaxis": {"title": "Date"},
                           "yaxis": {"title": "Total value (EUR)"},
                           "showlegend": False})
        plot(fig, filename='portfoliovalue.html')


if __name__ == '__main__':
    pfManager = PortfolioManager('testPortfolio.json', create=True)
    # txLogPath = r'C:\pythonProjecten\portfolio-tracker\logs\test.log'
    # pfManager.updatePortfolio(txLogPath)
    # pfManager.show()
    pfManager.requestHistoricalPrices()
    history = pfManager.getHistoricalPortfolioPrices()
    pfManager.plotHistoricalPortfolioValue()

    totalUSDValue, totalEURValue = pfManager.getTotalValue()

    print(80*'*')
    print("\tTotal Value in USD: {}\nTotal Value in EUR: {}".format(totalUSDValue, totalEURValue))
    print(80*'*')