# Portfolio Tracker

Tracks portfolio prices

## Getting started

The main elements are the transactionLogger and the portfolioManager.
The transactionLogger keeps track of a .log file with timestamped transactions.
The portfoliomanager keeps track of a .json file with the current holdings. These holdings are update when the LastUpdated date of this portfolio is earlier than a transaction.
This prevents doublecounting of transactions.

Then the portfolioManager can request all actual prices from CoinGecko and add them all up.

To share the results with your friends, call the Message Generator to create an emojified message to share!